import React, { useState, useEffect, useCallback } from 'react'


const MyToken = () => {
    const [listToken, setListToken] = useState([])

    useEffect(() => {
        const getToken = async () => {
            const data = await window.createTokenContract.get_tokens(
               { from_index: 0, limit: 100 }
             );
             const myData = data.filter(x => x.owner_id  === window.accountId)
             setListToken(myData)
             console.log('data', data)
             console.log('data', window.accountId)
           };
           getToken()
    }, [])
    
  return (
   <div>
       <div>My Token</div>
       {listToken?.map(((item, index) =>{
           return (
               <div style = {{marginTop: 16, alignItems: 'center', justifyContent: 'space-between', backgroundColor: 'gray', borderRadius: 20}} className='d-flex' key = {index}>
                   <img src = {item.metadata.icon || 'https://s2.coinmarketcap.com/static/img/coins/200x200/6535.png'}  style = {{ height: 46, width: 46, borderRadius: 80, margin: 32}}></img>
                   <div>
                       <div style = {{fontSize: 12}}>Symbol*</div>
                   <div style = {{marginLeft: 16, fontWeight: 'bold', fontSize: 18}}> {item.metadata.symbol}</div>
                   <div style = {{fontSize: 12, marginTop: 8}}>Name*</div>
                   <div style = {{marginLeft: 16}}> {item.metadata.name}</div>
                   </div>
                   <div>
                   <div style = {{fontSize: 12, marginTop: 8}}>Token supply*</div>
                   <div  style = {{marginLeft: 16, width: 300}}>{item.total_supply}</div>
                   <div style = {{fontSize: 12, marginTop: 8}}>Token id*</div>
                   <div style = {{marginLeft: 16}}> {item.metadata.symbol}.vuongnt.testnet</div>
                   </div>
                  
               </div>
            
           )
       }))}
   </div>
  )
}

export default MyToken