import React, { Component, useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import { parseNearAmount } from "near-api-js/lib/utils/format";
import BN from 'bn.js';

export default function Dropdowns() {
  const [name, setName] = useState();
  const [symbol, setSymbol] = useState();
  const [totalSupply, setTotalSupply] = useState();
  const [decimals, setDecimals] = useState();
  const [linkIcon, setLinkIcon] = useState('');
  const toBN = (totalSupply, decimals) => new BN(totalSupply).mul(new BN('10').pow(new BN(decimals)))
  useEffect(() => {
    const getToken = async () => {
     const data = await window.createTokenContract.get_tokens(
        { from_index: 0, limit: 100 }
      );
      console.log('data', data)
    };
    getToken()
  }, []);

  const createToken = async () => {
    if (
      name &&
      symbol &&
      decimals &&
      totalSupply &&
      window.walletConnection.isSignedIn()
    ) {
      await window.createTokenContract.new(
        {
          args: {
            owner_id: window.accountId,
            total_supply: totalSupply,
            metadata: {
              spec: "ft-1.0.0",
              name,
              symbol,
              decimals: parseInt(decimals),
              icon:linkIcon
            },
          },
        }
      );
    } else {
      alert("error information or connect wallet! ");
    }
  };
  return (
    <div className="col-12 grid-margin stretch-card">
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">Create your token</h4>
          <form className="forms-sample">
            <Form.Group>
              <label htmlFor="token">Token Name*</label>
              <Form.Control
                type="text"
                className="form-control"
                id="exampleInputName1"
                placeholder="Ex: Ethereum"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <label htmlFor="amount">Token Symbol*</label>
              <Form.Control
                type="text"
                className="form-control"
                id="exampleInputName2"
                placeholder="Ex: ETH"
                value={symbol}
                onChange={(e) => setSymbol(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <label htmlFor="amount">Link Icon*</label>
              <Form.Control
                type="text"
                className="form-control"
                id="exampleInputName3"
                placeholder="Ex: http://..."
                value={linkIcon}
                onChange={(e) => setLinkIcon(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <label htmlFor="amount">Decimals*</label>
              <Form.Control
                type="number"
                className="form-control"
                id="exampleInputName4"
                placeholder="Ex: 18"
                value={decimals}
                onChange={(e) => setDecimals(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <label htmlFor="amount">Total supply*</label>
              <Form.Control
                type="number"
                className="form-control"
                id="exampleInputName5"
                placeholder="Ex: 100000000"
                value={totalSupply}
                onChange={(e) => setTotalSupply(e.target.value)}
              />
            </Form.Group>
            <div></div>
          </form>
          <button
            type="submit"
            className="btn btn-primary mr-2 mt-2"
            onClick={() => createToken()}
          >
            Create Token
          </button>
        </div>
      </div>
    </div>
  );
}
