import { Contract } from 'near-api-js'
import React, { useEffect, useState } from 'react'
import moment from 'moment';

const LaunchItem = ({ item, index }) => {
    const [countTime, setCountTime] = useState(0)
    const process = item.total_token_sold / item.amount * 100

    useEffect(() => {
        setInterval(() => {
            const now = new Date()
            setCountTime(item.start_block / 1000000 - now.getTime())
        }, 1000);
    }, [])
    return (
        <div className="col-sm-4 grid-margin" key={index.toString()}>
            <div className="card">
                <div className="card-body">
                    <div className="row">
                        <div className="col-8 col-sm-12 col-xl-8 my-auto" style={{ alignItems: 'center' }}>
                            <h3>DRV</h3>
                            <h3><small className="text-muted">1 NEAR = {item.token_price} DRV</small></h3>
                        </div>
                        <div className="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                            <div className="preview-thumbnail">
                                <img src={item.logo_url} style={{ width: 64 }} alt="face" className="rounded-circle" />
                            </div>
                        </div>
                    </div>

                    <br />
                    <label>Soft/Hard Cap:</label>
                    <h2 className="mb-0">${item.soft_cap} NEAR - {item.hard_cap} NEAR</h2>
                    <br />
                    <label>Progress ({process}%)</label>
                    <div className="progress progress-md portfolio-progress">
                        <div className="progress-bar bg-success" role="progressbar" style={{ width: `${process}%` }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <br />
                    <h6>Liquidity %: <span className="text-muted">{item.liquidity_percent} %</span></h6>
                    <h6>Lockup Time: <span className="text-muted">{item.lock_period} minutes</span></h6>
                    <div className=" border-bottom" >
                        <br />
                    </div>
                    <br />
                    <div className="row" style={{ alignItems: 'center' }} >
                        <div className="col-8 col-sm-12 col-xl-8 my-auto">
                            <label>Sale Starts In:</label>
                            <h6>{moment(countTime).format('HH:mm:ss')}</h6>
                        </div>
                        <div className="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                            <button type="button" className="btn btn-primary btn-rounded ml-auto">View Pool</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default function ListLaunch() {

    const [presaleData, setPresaleData] = useState([])

    useEffect(() => {
        getListPresale()
    }, [])

    const getListPresale = async () => {
        try {
            const data = await window.factoryContract.get_presales({
                from_index: 0,
                limit: 10
            })
            for (const item of data) {
                console.log('item.presale_account_id', item.presale_account_id)
                let contract = await new Contract(window.walletConnection.account(), item.presale_account_id, {
                    viewMethods: ['get_info'],
                })
                const launchpadData = await contract.get_info({})
                const clonePresaleData = [...presaleData]
                clonePresaleData.push(launchpadData)
                console.log('clonePresaleData', clonePresaleData)
                setPresaleData(clonePresaleData)
            }
        } catch (error) {
            console.log('error', error)
        }
    }
    console.log('presaleData', presaleData)
    return (
        <div>
            <h1>Current Presale</h1>
            <div className="row">
                {presaleData.map((e, i) => <LaunchItem item={e} index={i} />)}
            </div>
        </div>
    )
}
